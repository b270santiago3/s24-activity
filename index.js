function getCube(num) {
  const cube = num ** 3;
  return cube;
}

console.log(`The cube of 2 is ${getCube(2)}`);

const address = [258, "Washington Ave NW", "California", 9011];

const [houseNumber, streetName, state, zipCode] = address;
console.log(`I live at ${houseNumber} ${streetName}, ${state} ${zipCode} `);

let animals = {
	animalsName: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

let {animalsName, species, weight, measurement} = animals;
console.log(`${animalsName} was a ${species}. He ${weight} with a measurement of ${measurement}.`);

const numbers = [1, 2, 3, 4, 5];
numbers.forEach((number) => console.log(number));

const reduceNumber = numbers.reduce((allNumber, totalValue) => allNumber + totalValue);

console.log(reduceNumber);

class Dog {
  constructor(name, age, breed) {
    this.name = name;
    this.age = age;
    this.breed = breed;
  }
}

const dogDetails = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(dogDetails);
